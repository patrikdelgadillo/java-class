import java.util.HashSet;
import java.util.Set;

public class MainProgram {
    public static void main(String[] args) {
        Set<Person> persons = new HashSet<Person>();

        Person p1 = new Person("Nicolas",30);
        Person p2 = new Person("Juan",38);
        Person p3 = new Person("Diego",33);
        Person p4 = new Person("Martin",25);
        Person p5 = new Person("Diego",33);

        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        persons.add(p5);

        System.out.println(persons);
    }
}
